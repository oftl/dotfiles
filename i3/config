# Mod1 ... alt
# Mod4 ... win
# set $modN Mod1+Shift
# set $modc Ctrl
# set $modc Shift+Ctrl

set $modA Mod1
set $modC Ctrl

# set $shift Shift
set $shift 94  # hacky shift on de keyboard
set $shift_l 50
set $shift_r 62
# bindsym $shift exec "i3-nagbar -t warning -m 'shift'"
# bind $shift_l exec "i3-nagbar -t warning -m 'shift l'"
# bind $shift_r exec "i3-nagbar -t warning -m 'shift r'"

# UI
focus_follows_mouse no
font pango:monospace 14

# all colors from
# https://github.com/tobiaszwaszak/i3wm/blob/master/config
client.focused          #002b36 #586e75 #fdf6e3 #268bd2
client.focused_inactive #002b36 #073642 #839496 #073642
client.unfocused        #002b36 #073642 #839496 #073642
client.urgent           #002b36 #dc322f #fdf6e3 #002b36

for_window [class="MPlayer"] floating enable
for_window [class="mpv"] floating enable
for_window [class="feh"] floating enable

# https://wiki.archlinux.org/index.php/i3#Screensaver_and_power_management
# exec --no-startup-id xset dpms 600 &

workspace_auto_back_and_forth yes

###
### most common bindings

# terminal
bindsym $modC+Return exec i3-sensible-terminal

# lock screen
bindsym $modC+Escape exec ~/bin/o3lock ; mode "default"
bindsym $modC+Tab exec "systemctl suspend && ~/bin/o3lock" ; mode "default"

# change focus
# bindsym $modA+j focus down
# bindsym $modA+k focus up
# bindsym $modA+l focus right
# bindsym $modA+h focus left

# switch to workspace
# bindsym $modA+1 workspace 1
# bindsym $modA+2 workspace 2
# bindsym $modA+3 workspace 3
# bindsym $modA+4 workspace 4
# bindsym $modA+5 workspace 5
# bindsym $modA+6 workspace 6
# bindsym $modA+7 workspace 7
# bindsym $modA+8 workspace 8
# bindsym $modA+9 workspace 9

###
### main mode (command mode)

# bindsym $modC+w mode "C O M M A N D"
# bindsym $modC+e mode "C O M M A N D"
# bindsym $modC+s mode "C O M M A N D"
bindsym $modC+q mode "C O M M A N D"
bindsym $modC+i mode "C O M M A N D"

mode "C O M M A N D" {
    # these most common bindings are available outside command-mode too

    # terminal
    bindsym Return exec i3-sensible-terminal ; mode "default"

    # menu
    bindsym d exec dmenu_run ; mode "default"

    #####

    bindsym a exec "i3-nagbar -t warning -m 'mod i '"

    # change focus
    bindsym j focus down ; mode "default"
    bindsym k focus up ; mode "default"
    bindsym l focus right ; mode "default"
    bindsym h focus left ; mode "default"

    # move focused window
    bindsym shift+h move left
    bindsym shift+j move down
    bindsym shift+k move up
    bindsym shift+l move right

    bindsym Return mode "default"
    bindsym Escape mode "default"

    # restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
    bindsym shift+r restart ; mode "default"

    # reload the configuration file
    bindsym c reload ; mode "default"

    # kill focused window
    bindsym x kill ; mode "default"

    # kill focused window
    bindsym x kill ; mode "default"

    # split in horizontal orientation
    bindsym w split h ; mode "default"

    # split in vertical orientation
    bindsym v split v ; mode "default"

    # enter fullscreen mode for the focused container
    bindsym f fullscreen toggle ; mode "default"

    # toggle floating/tiling
    bindsym shift+f floating toggle ; mode "default"

    # change focus between tiling / floating windows
    bindsym shift+space focus mode_toggle ; mode "default"

    # scratchpad
    bindsym shift+s move scratchpad ; mode "default"
    bindsym i scratchpad show ; mode "default"

    # change container layout (stacked, tabbed, toggle split)
    bindsym s layout stacking ; mode "default"
    bindsym t layout tabbed ; mode "default"
    bindsym e layout toggle split ; mode "default"

    # focus the parent container
    bindsym p focus parent ; mode "default"

    # focus the child container
    bindsym c focus child ; mode "default"

    # switch to workspace
    bindsym 1 workspace 1; mode "default"
    # bindsym 1 workspace comms; mode "default"
    bindsym 2 workspace 2; mode "default"
    bindsym 3 workspace 3; mode "default"
    bindsym 4 workspace 4; mode "default"
    bindsym 5 workspace 5; mode "default"
    bindsym 6 workspace 6; mode "default"
    bindsym 7 workspace 7; mode "default"
    bindsym 8 workspace 8; mode "default"
    bindsym 9 workspace 9; mode "default"

    # move focused container to workspace
    bindsym shift+1 move container to workspace 1; mode "default"
    # bindsym shift+1 move container to workspace comms; mode "default"
    bindsym shift+2 move container to workspace 2; mode "default"
    bindsym shift+3 move container to workspace 3; mode "default"
    bindsym shift+4 move container to workspace 4; mode "default"
    bindsym shift+5 move container to workspace 5; mode "default"
    bindsym shift+6 move container to workspace 6; mode "default"
    bindsym shift+7 move container to workspace 7; mode "default"
    bindsym shift+8 move container to workspace 8; mode "default"
    bindsym shift+9 move container to workspace 9; mode "default"

    bindsym m mode "M O V E"
    bindsym r mode "R E S I Z E"
    bindsym o mode "M O U S E"

    # alt keyboard mappings (.xprofile seems to be forgotten afterwards)
    bindsym shift+d exec setxkbmap de ; mode "default"
    bindsym shift+u exec setxkbmap us ; mode "default"

    # exit i3 (logs you out of your X session)
    bindsym shift+x exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
}

###
### resize mode

# this is still somewhat buggy (also, not really used)
mode "R E S I Z E" {
        bindsym h resize shrink left  10 px
        bindsym j resize shrink down  10 px
        bindsym k resize shrink up    10 px
        bindsym l resize shrink right 10 px


        bindsym shift+h resize grow left  10 px
        bindsym shift+j resize grow down  10 px
        bindsym shift+k resize grow up    10 px
        bindsym shift+l resize grow right 10 px

        # small
        bindsym s resize set width 640 px height 480 px; move absolute position center
        # x-large
        bindsym x resize set width 1480 px height 900 px; move absolute position center

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

###
### move mode

mode "M O V E" {
        bindsym h move left 50 px
        bindsym j move down 50 px
        bindsym k move up 50 px
        bindsym l move right 50 px

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "M O U S E" {
        # up
        bindsym k       exec "xdotool mousemove_relative --polar 0 10"
        bindsym Shift+k exec "xdotool mousemove_relative --polar 0 100"
        # right
        bindsym l       exec "xdotool mousemove_relative --polar 90 10"
        bindsym Shift+l exec "xdotool mousemove_relative --polar 90 100"
        # down
        bindsym j       exec "xdotool mousemove_relative --polar 180 10"
        bindsym Shift+j exec "xdotool mousemove_relative --polar 180 100"
        # left
        bindsym h       exec "xdotool mousemove_relative --polar 270 10"
        bindsym Shift+h exec "xdotool mousemove_relative --polar 270 100"

        # mouse buttons
        bindsym Alt_L exec "xdotool click 1"
        bindsym Space exec "xdotool click 2"
        bindsym Alt_R exec "xdotool click 3"

        # scroll up
        bindsym u exec "xdotool click 4"
        # scroll down
        bindsym i exec "xdotool click 5"

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# # up
# bindsym $mouse+w exec "xdotool mousemove_relative --polar 0 100"
# # right
# bindsym $mouse+d exec "xdotool mousemove_relative --polar 90 100"
# # down
# bindsym $mouse+s exec "xdotool mousemove_relative --polar 180 100"
# # left
# bindsym $mouse+a exec "xdotool mousemove_relative --polar 270 100"

###
### bars

bar {
    id bar-top
    status_command i3status --config ~/.config/i3/status_top.conf
    position top
    separator_symbol " / "
    colors {
        background #002b36
        statusline #839496
        separator  #586e75
        focused_workspace  #b58900 #b58900 #002b36
        active_workspace   #586e75 #586e75 #002b36
        inactive_workspace #073642 #002b36 #839496
        urgent_workspace   #dc322f #dc322f #fdf6e3
    }
}

bar {
    id bar-bottom
    status_command i3status --config ~/.config/i3/status_bottom.conf
    position bottom
    separator_symbol " / "
    workspace_buttons no
    colors {
        background         #002b36
        statusline         #839496
        separator          #586e75
        focused_workspace  #b58900 #b58900 #002b36
        active_workspace   #586e75 #586e75 #002b36
        inactive_workspace #073642 #002b36 #839496
        urgent_workspace   #dc322f #dc322f #fdf6e3
    }
}
