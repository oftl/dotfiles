" hide NERDTree/tagbar on :Gblame (and possibly others)
" sync open/open-focus/others? between nerdtree and tagbar

" change light/dark of (n)vim and urxvt togetter

source $HOME/.config/nvim/plug.vim

let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')

if !filereadable(vimplug_exists)
    if !executable("curl")
        echoerr "You have to install curl or first install vim-plug yourself!"
        execute "q!"
    endif
    echo "Installing Vim-Plug..."
    echo ""
    silent !\curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    let g:not_finish_vimplug = "yes"

    autocmd VimEnter * PlugInstall
endif

let mapleader=','
let maplocalleader='\'

call plug#begin(expand('~/.config/nvim/plugged'))

let g:python_host_prog = "/usr/bin/python3"

"*****************************************************************************
"" Plug install packages
"*****************************************************************************

" Plug 'vim-scripts/EasyMotion'
Plug 'justinmk/vim-sneak'

Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'

let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=0
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 50
let g:NERDTreeWinPos = 'left'

Plug 'majutsushi/tagbar'

let g:tagbar_autofocus = 1
let g:tagbar_vertical = 25
let g:tagbar_left = 1

nnoremap <Leader>nt :NERDTreeToggle <CR> :TagbarToggle <CR>
noremap <Leader>n :NERDTreeTabsToggle <CR>
noremap <Leader>t :TagbarToggle <CR>
" nnoremap <silent> <F2> :NERDTreeFind<CR>

Plug 'tpope/vim-commentary'

Plug 'ntpeters/vim-better-whitespace'

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

Plug 'Raimondi/delimitMate'             " <S-Tab> jump over

let delimitMate_expand_space=1
let delimitMate_expand_cr = 1

Plug 'ludovicchabant/vim-gutentags'

let g:gutentags_ctags_exclude = ['.cache', '.yarn', '.git', '.svn']

Plug 'Yggdroot/indentLine'

let g:indentLine_bufTypeExclude = ['help', 'terminal']
let g:indentLine_bufNameExclude = ['NERDTree*', '__Tagbar__*']

let g:indentLine_char = '┆'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim' " pacman -S community/fzf


Plug 'wincent/ferret'   " pacman -S ripgrep

noremap <Leader>a :Ack

Plug 'wincent/loupe'    " enhance search

" fzf, mnemonic: <f>ind file
noremap <silent><Leader>f :Files<CR>
" ferret, mnemonic: <a>ck (like perl-ack)
noremap <silent><Leader>a :Ack <CR>
"
" ?!? " cnoremap <C-P> <C-R>=expand("%:p:h") . "/" <CR>
nnoremap <silent> <Leader>e :FZF -m<CR>
"" fzf.vim
let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"

" ripgrep
if executable('rg')
    let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
    set grepprg=rg\ --vimgrep
    command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
endif

" set path=.,**
" nnoremap <Leader>f :find *
" nnoremap <Leader>s :sfind *
" nnoremap <Leader>v :vert sfind *
" nnoremap <Leader>t :tabfind *
" nnoremap <Leader>F :find <C-R>=expand('%:h').'/*'<CR>
" nnoremap <Leader>S :sfind <C-R>=expand('%:h').'/*'<CR>
" nnoremap <Leader>V :vert sfind <C-R>=expand('%:h').'/*'<CR>
" nnoremap <Leader>T :tabfind <C-R>=expand('%:h').'/*'<CR>

" vim-Session
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'

let g:session_directory = "~/.config/nvim/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1

" nnoremap <Leader>so :OpenSession<Space>
" nnoremap <Leader>ss :SaveSession<Space>
" nnoremap <Leader>sd :DeleteSession<CR>
" nnoremap <Leader>sc :CloseSession<CR>

" snippets (1)
"
" Plug 'Shougo/deoppet.nvim', { 'do': ':UpdateRemotePlugins' }

" snippets (2)
"
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<shift-tab>"
let g:UltiSnipsEditSplit="vertical"

"" :set paste automatically
"Plug 'ConradIrwin/vim-bracketed-paste'

"" Plug 'roxma/nvim-completion-manager'
"" language server
"" Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins'  }
""
"Plug 'Shougo/deoplete.nvim'

"" let g:deoplete#enable_at_startup = 1
"let g:deoplete#auto_complete = 1

"" call deoplete#custom#option('smart_case', v:true)

"" " <C-h>, <BS>: close popup and delete backword char.
"" inoremap <expr><C-h> deoplete#smart_close_popup()."\<C-h>"
"" inoremap <expr><BS>  deoplete#smart_close_popup()."\<C-h>"

"" " <CR>: close popup and save indent.
"" inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
"" function! s:my_cr_function() abort
"" return deoplete#close_popup() . "\<CR>"
"" endfunction

"let g:deoplete#ignore_sources = get(g:, 'deoplete#ignore_sources', {})
"let g:deoplete#auto_complete = 1
"let g:deoplete#enable_at_startup = 1
"let g:deoplete#auto_complete = 1

"" call deoplete#custom#option('smart_case', v:true)

"" " <C-h>, <BS>: close popup and delete backword char.
"" inoremap <expr><C-h> deoplete#smart_close_popup()."\<C-h>"
"" inoremap <expr><BS>  deoplete#smart_close_popup()."\<C-h>"

"" " <CR>: close popup and save indent.
"" inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
"" function! s:my_cr_function() abort
"" return deoplete#close_popup() . "\<CR>"
"" endfunction

"let g:deoplete#ignore_sources = get(g:, 'deoplete#ignore_sources', {})
"let g:deoplete#auto_complete = 1

" Plug 'ElmCast/elm-vim'
Plug 'theJian/elm.vim'

" " let g:elm_setup_keybindings = 0
" " let g:elm_format_autosave = 1
" let g:elm_jump_to_error = 1
" let g:elm_make_output_file = "elm.js"
" let g:elm_make_show_warnings = 0
" let g:elm_syntastic_show_warnings = 0
" let g:elm_browser_command = ""
" let g:elm_detailed_complete = 0
" let g:elm_format_autosave = 1
" let g:elm_format_fail_silently = 0
" let g:elm_setup_keybindings = 1

" haskell
Plug 'eagletmt/neco-ghc'
Plug 'dag/vim2hs'
Plug 'pbrisbin/vim-syntax-shakespeare'

" let g:haskell_conceal_wide = 0
" let g:haskell_multiline_strings = 1
" let g:necoghc_enable_detailed_browse = 1
" autocmd Filetype haskell setlocal omnifunc=necoghc#omnifunc

" javascript
" Plug 'jelera/vim-javascript-syntax'
" JavaScript bundle for vim, this bundle provides syntax highlighting and improved indentation.
" Plug 'pangloss/vim-javascript'
"
" typescript
" Plug 'mhartington/nvim-typescript'

Plug 'w0rp/ale'
" Plug 'mrk21/yaml-vim'
Plug 'adrienverge/yamllint'

" highlight ALEError ctermbg=8 ctermfg=200
" highlight ALEWarning ctermbg=8 ctermfg=208

let g:ale_sign_column_always = 1
let g:ale_sign_error = 'E'
let g:ale_sign_warning = 'W'
let g:ale_fix_on_save = 1
let g:ale_lint_on_insert_leave = 1

let g:ale_fixers = {
\    'python': ['black','isort'],
\    'generic': ['remove_trailing_lines', 'trim_whitespace'],
\    'elm': ['elm-format'],
\    'json': ['prettier'],
\}
" eslint does not work :(

let g:ale_linters = {
\    'python': ['flake8', 'isort', 'mypy'],
\}

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

nnoremap <Leader>af :ALEFix<CR>
nnoremap <Leader>an :ALENextWrap<CR>
nnoremap <Leader>ap :ALEPreviousWrap<CR>
nnoremap <C-n> :ALENextWrap<CR>
nnoremap <C-p> :ALEPreviousWrap<CR>

highlight ALEError cterm=bold ctermbg=8 ctermfg=202
highlight ALEWarning cterm=bold ctermbg=8 ctermfg=202
highlight ALEInfo cterm=bold ctermbg=8 ctermfg=142
highlight ALEStyleError cterm=bold ctermbg=8 ctermfg=227
highlight ALEStyleWarning cterm=bold ctermbg=8 ctermfg=227

Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'

function! LightlineReadonly()
  return &readonly && &filetype !=# 'help' ? 'RO' : ''
endfunction

let g:lightline = {
    \ 'colorscheme': 'solarized',
    \ 'active': {
    \   'left': [
    \       [ 'mode', 'paste' ],
    \       [ 'readonly', 'filename', 'modified'],
    \       [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok' ],
    \   ],
    \   'right': [
    \       [ 'git', 'fileformat', 'fileencoding', 'filetype', 'virtual_env'],
    \       [ 'lineinfo', 'percent' ],
    \       [ 'position', 'charvalue' ],
    \   ]
    \ },
    \ 'component': {
    \   'charvalue': '%b 0x%B',
    \   'position': '%o 0x%O',
    \ },
    \ 'component_function': {
    \   'git':         'fugitive#statusline',
    \   'readonly':    'LightlineReadonly',
    \   'virtual_env': 'virtualenv#statusline',
    \ },
    \ 'component_expand': {
    \   'linter_checking':  'lightline#ale#checking',
    \   'linter_warnings':  'lightline#ale#warnings',
    \   'linter_errors':    'lightline#ale#errors',
    \   'linter_ok':        'lightline#ale#ok',
    \ },
    \ 'component_type': {
    \   'linter_checking': 'left',
    \   'linter_warnings': 'warning',
    \   'linter_errors': 'error',
    \   'linter_ok': 'left',
    \ },
    \ }

" Asynchronous linting and make framework for Neovim/Vim
Plug 'neomake/neomake'

" call neomake#configure#automake('w')

Plug 'davidhalter/jedi-vim'

let g:jedi#completions_command = "<C-N>"
let g:jedi#show_call_signatures = 2
let g:jedi#force_py_version = 3

" TODO make dependent on screensize
" let g:jedi#use_tabs_not_buffers=1
" let g:jedi#use_splits_not_buffers = "right"

let g:jedi#goto_command = "<Leader>jg"
let g:jedi#goto_assignments_command = "<Leader>ja"
let g:jedi#goto_definitions_command = "<Leader>jd"
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<Leader>ju"
let g:jedi#rename_command = "<Leader>jr"

" requirements syntax
Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}
Plug 'plytophogy/vim-virtualenv'

let g:virtualenv_directory = '~/venv/'
let g:virtualenv_auto_activate = 1

Plug 'fisadev/vim-isort'
Plug 'ivanov/vim-ipython'

Plug 'Chiel92/vim-autoformat'
" let g:autoformat_retab = 0
" au BufWrite * :Autoformat
" autocmd FileType vim,proto let b:autoformat_autoindent=0
" nnoremap <Leader>af :Autoformat

" python highlight included
Plug 'sheerun/vim-polyglot'
" included in polyglot
" Plug 'ekalinin/Dockerfile.vim'

" Plug 'tmux-plugins/vim-tmux'
Plug 'christoomey/vim-tmux-navigator'

let g:tmux_navigator_no_mappings = 1

" Switching windows with tmux
nnoremap <silent> <C-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-j> :TmuxNavigateDown<cr>
nnoremap <silent> <C-k> :TmuxNavigateUp<cr>
nnoremap <silent> <C-l> :TmuxNavigateRight<cr>
nnoremap <silent> <C-\> :TmuxNavigatePrevious<cr>

" Switching windows without tmux
" noremap <C-j> <C-w>j
" noremap <C-k> <C-w>k
" noremap <C-l> <C-w>l
" noremap <C-h> <C-w>h

Plug 'benmills/vimux'
map <Leader>tt :VimuxRunCommand("top") <CR>
map <Leader>tu :VimuxRunCommand("uptime") <CR>
map <Leader>tc :VimuxPromptCommand <CR>
map <Leader>t. :VimuxRunLastCommand <CR>
map <Leader>tq :VimuxInterruptRunner <CR>
map <Leader>tz :VimuxZoomRunner<CR>

" function! LocalVimrc()
"     let l:local_vimrc = findfile("local.vimrc", ".;")
"     echo local_vimrc
"     " echo "done"
"     " if filereadable(expand("./vimrc"))
"     "     source "./vimrc"
"     " endif
" endfunction

function! VimuxSlime()
 call VimuxSendText(@v)
 call VimuxSendKeys("Enter")
endfunction

" If text is selected, save it in the v buffer and send that buffer it to tmux
vmap <LocalLeader>ts "vy :call VimuxSlime()<CR>

" Select current paragraph and send it to tmux
nmap <LocalLeader>ts vip<LocalLeader>ts<CR>

Plug 'embear/vim-localvimrc'
let g:localvimrc_name = "local.vimrc"

" Plug 'vim-scripts/YankRing.vim'   " access yank registers

Plug 'amiorin/vim-project'

let g:project_use_nerdtree = 1
let g:project_enable_welcome = 0
" source $HOME/.config/nvim/project.vim

Plug 'mhinz/vim-startify'

Plug 'vim-ctrlspace/vim-ctrlspace'

" ctrl-space
nnoremap <silent><leader><Space> :CtrlSpace<CR>
let g:CtrlSpaceSaveWorkspaceOnExit = 1
let g:CtrlSpaceSaveWorkspaceOnSwitch = 1
let g:CtrlSpaceLoadLastWorkspaceOnStart = 1

" VCS
Plug 'jreybert/vimagit'
nnoremap <silent><Leader>m :MagitOnly<CR>

Plug 'gregsexton/gitv'
Plug 'tpope/vim-rhubarb'           " assist fugitive
Plug 'vim-scripts/vcscommand.vim'  " CVS, SVN, SVK, git, bzr, and hg

Plug 'tpope/vim-fugitive'

if exists("*fugitive#statusline")
    set statusline+=%{fugitive#statusline()}
endif

Plug 'mhinz/vim-signify'

""" signify
let g:signify_vcs_list = [ 'git', 'svn' ]

nnoremap <Leader>gt :SignifyToggle<CR>
nnoremap <Leader>gh :SignifyToggleHighlight<CR>
nnoremap <Leader>gr :SignifyRefresh<CR>
nnoremap <Leader>gd :SignifyDebug<CR>

" Plug 'airblade/vim-gitgutter'     " if git only

" :set paste automatically
Plug 'ConradIrwin/vim-bracketed-paste'

Plug 'tpope/vim-surround'
Plug 'vim-scripts/matchit.zip'

Plug 'myusuf3/numbers.vim'

let g:numbers_exclude = ['help', 'nerdtree', 'unite', 'tagbar', 'startify', 'gundo', 'vimshell', 'w3m', 'vundle', 'term']

" close <tags> when </ is encounterd or C-_
Plug 'docunext/closetag.vim'

" let plugins do .!
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-jdaddy'
Plug 'tpope/vim-speeddating'  " better <c-a> <c-z>

" Plug 'sjl/gundo.vim'
" noremap <Leader>g :GundoToggle <CR>

Plug 'mbbill/undotree'

noremap <Leader>g :UndotreeToggle <CR>
let g:undotree_SplitWidth = 50

" Plugin to toggle, display and navigate marks
Plug 'kshenoy/vim-signature'

"" http://vim.wikia.com/wiki/Improved_hex_editing
Plug 'fidian/hexmode'

Plug 'ryanoasis/vim-devicons'

" xdebug integration
" AUR python-pydbgp
" https://pypi.org/project/komodo-python3-dbgp/
Plug 'joonty/vdebug'

""" MIXED """

"" vdebug and python, a match made somewhere else :/
" pacman -S aur/python-pydbgp
" according to :help Vdebug
" cd /usr/bin/
" sudo ln -s /usr/lib/python3.6/site-packages/dbgp
"
" F5/F6
" python -S /usr/bin/pydbgp -d localhost:9000 %&

" let g:python_host_prog  = '/usr/bin/python'
" let g:python3_host_prog = '/usr/bin/python3'
" disable python 2
" let g:loaded_python_provider = 1

let python_highlight_all = 1

" augroup vimrc-python
"     autocmd!
"     autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=0
"     " autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8
"         \ formatoptions+=croq softtabstop=4
"         \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
" augroup END


Plug 'altercation/vim-colors-solarized'

call plug#end()

"*****************************************************************************
"" Basic Setup
"*****************************************************************************"
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

"" Fix backspace indent
set backspace=indent,eol,start

"" Directories for swp files
set nobackup
set noswapfile

" various plugins need this
set nocompatible

"" Enable hidden buffers
set hidden
set autoindent
set smartindent
set cindent
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" set filetype=on # turned on be next ones
filetype plugin on
filetype indent on

""" searching
set incsearch
set smartcase  " ignore case for only-lowercase words
set hlsearch
noremap <Space> :set hlsearch! hlsearch?<CR>  " turn highlight of search on/off

" These will make it so that going to the next one in a search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

""" UI
set showcmd
set wildmenu
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__,*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
set shiftwidth=4
set showmatch   " jump to previous { when inserting }

""" show tabs
set listchars=tab:>-
set list

set fileformat=unix
set fileformats=unix,dos,mac

if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" Required:
filetype plugin indent on

" set statusline=%F%m%r%h%w%=[%p%%]\ [%l/%c]\ [%b/0x%B]\ [%f/%{&ff}/%Y]

set noshowmode

"*****************************************************************************
"" Visual Settings
"*****************************************************************************
syntax on
set ruler
set number

let no_buffers_menu=1

"" Disable the blinking cursor.
set gcr=a:blinkon0
set scrolloff=3

" " If cursor is in first or last line of window, scroll to middle line.
" function s:MaybeMiddle()
"   if winline() == 1 || winline() == winheight(0)
"     normal! zz
"   endif
" endfunction
"
" nnoremap <silent> n n:call <SID>MaybeMiddle()<CR>
" nnoremap <silent> N N:call <SID>MaybeMiddle()<CR>

"" Status bar
set laststatus=2

"" Use modeline overrides
set modeline
set modelines=10

" set title
set titleold="Prosole"
set titlestring=%F

" long line
set wrap
set wrapmargin=2
set linebreak
set breakindent
set showbreak="b> "
" set textwidth=79

""" visual bindings, prefixed with \

nnoremap <silent>\bd :set background=dark<CR>
nnoremap <silent>\bl :set background=light<CR>

set cursorline
nnoremap <silent>\l :set cursorline! <CR>
" set cursorcolumn
nnoremap <silent>\c :set cursorcolumn! <CR>

" terminal emulation
nnoremap  <silent> <Leader>sh :belowright 10split term://bash<CR>

"*****************************************************************************
"" Autocmd Rules
"*****************************************************************************
"
"" The PC is fast enough, do syntax highlight syncing from start unless 200 lines
augroup vimrc-sync-fromstart
    autocmd!
    autocmd BufEnter * :syntax sync maxlines=200
augroup END

"" Remember cursor position
augroup vimrc-remember-cursor-position
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

"" make/cmake
augroup vimrc-make-cmake
    autocmd!
    autocmd FileType make setlocal noexpandtab
    autocmd BufNewFile,BufRead CMakeLists.txt setlocal filetype=cmake
augroup END

augroup xml
    autocmd!
    autocmd FileType xml setlocal foldmethod=indent foldlevelstart=999 foldminlines=0
augroup END

set autoread
set autowriteall

"*****************************************************************************
"" Mappings
"*****************************************************************************
noremap <silent><Leader>, :blast<CR>

" pretty print xml sgml html
noremap <Leader>px :%s/></>\r</g<CR> gg=G<CR>

" highlight same word
noremap <Leader>h :exe "let HlUnderCursor=exists(\"HlUnderCursor\")?HlUnderCursor*-1+1:1"<CR>

" justify text (needs `par`)
noremap <Leader>aj vip :!par -w80 -j <CR>
" center
noremap <Leader>ac vip :center 80 <CR>
" right align
noremap <Leader>ar vip :right 80 <CR>
" spellchecking
noremap <Leader>sp :setlocal spell spelllang=de <CR>
noremap <Leader>spe :setlocal spell spelllang=en <CR>
noremap <Leader>sn :set nospell  <CR>

" remove trailing whitespace
noremap <Leader>tw :%s/ *$//g <CR>
" remove leading whitespace
noremap <Leader>tl :%s/^ *//g <CR>

" tabs
noremap <silent><Leader>tn :tabnew<CR>
noremap <silent><Leader>1 1gt<CR>
noremap <silent><Leader>2 2gt<CR>
noremap <silent><Leader>3 3gt<CR>
noremap <silent><Leader>4 4gt<CR>
noremap <silent><Leader>5 5gt<CR>
noremap <silent><Leader>6 6gt<CR>
noremap <silent><Leader>7 7gt<CR>
noremap <silent><Leader>7 8gt<CR>
noremap <silent><Leader>9 9gt<CR>

" buffers
noremap <silent><Leader>bl :buffers<CR>
noremap <silent><Leader>bn :bnext<CR>
noremap <silent><Leader>bp :bprevious<CR>
noremap <silent><Leader>b1 :buffer 1<CR>
noremap <silent><Leader>b2 :buffer 2<CR>
noremap <silent><Leader>b3 :buffer 3<CR>
noremap <silent><Leader>b4 :buffer 4<CR>
noremap <silent><Leader>b5 :buffer 5<CR>
noremap <silent><Leader>b6 :buffer 6<CR>
noremap <silent><Leader>b7 :buffer 7<CR>
noremap <silent><Leader>b8 :buffer 8<CR>
noremap <silent><Leader>b9 :buffer 9<CR>

" Goto Buffer!
nnoremap <Leader>bb :ls<CR>:b<Space>

"" Close buffer
noremap <Leader>bc :bd<CR>

nnoremap <silent> <Leader>b :Buffers<CR>

" quit quick
noremap <Leader>. :quit<CR>
noremap <Leader>w :w<CR>
noremap <Leader><Leader> <c-^>

noremap <silent><Leader>s :Startify <CR>
noremap <silent><Leader>p :Welcome <CR>

"" Split
noremap <Leader>s :split<CR>
noremap <Leader>v :vsplit<CR>
" vv to generate new vertical split
nnoremap <silent> vv <C-w>v

" gitv suggestions
nmap <Leader>gv :Gitv --all<cr>
set lazyredraw

" numbers
noremap <Leader>nn :NumbersToggle<CR>

"" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>ee :e <C-R>=expand("%:p:h") . "/" <CR>

"" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

"" Copy/Paste/Cut
if has('unnamedplus')
    set clipboard=unnamed,unnamedplus
endif

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

"" colorscheme and fixes for it, should stay last in init.vim

colorscheme solarized
set background=dark

" color tweaks to go with solarized
" note: these are for dark backgound

highlight SignifySignAdd    cterm=bold ctermbg=8  ctermfg=119
highlight SignifySignDelete cterm=bold ctermbg=8  ctermfg=167
highlight SignifySignChange cterm=bold ctermbg=8  ctermfg=227
"" empty sign column lines
highlight SignColumn        cterm=bold ctermbg=8
"" number's column
highlight LineNr            cterm=bold ctermbg=8

imap jk <Esc>
