# install dotfiles

if [ -e bin/install.sh.conf ]; then
    echo "loading config form bin/install.sh.conf"
    source bin/install.sh.conf
else
    echo "config missing"
    exit -1
fi

cd ${DOTFILES}

if [[ ${DOTFILES} == "" ]]; then
    echo "missing \$DOTFILES must edit install.sh.config"
    exit -1
fi

if [[ ${BACKUP} == "" ]]; then
    echo "missing \$BACKUP must edit install.sh.config"
    exit -1
fi

if [[ ! -d ${BACKUP} ]]; then
    ${MKDIR} -p ${BACKUP}
fi

### tmux
if [[ -e ${HOME}/.tmux.conf ]]; then
    echo "backup .tmux.conf"
    ${COPY} ${HOME}/.tmux.conf ${BACKUP}/tmux.conf
fi

echo "install .tmux.conf"
${COPY} ${DOTFILES}/tmux.conf ${HOME}/.tmux.conf

echo "make sure to have xclip in your PATH for copy/paste"
echo "more options include: tmuxinator, tmate"

### xprofile
if [[ -e ${HOME}/.xprofile ]]; then
    echo "backup .xprofile"
    ${COPY} ${HOME}/.xprofile ${BACKUP}/xprofile
fi
echo "install .xprofile"
${COPY} ${DOTFILES}/xprofile ${HOME}/.xprofile

### inputrc
if [[ -e ${HOME}/.inputrc ]]; then
    echo "backup .inputrc"
    ${COPY} ${HOME}/.inputrc ${BACKUP}/xprofile
fi
echo "install .inputrc"
${COPY} ${DOTFILES}/inputrc ${HOME}/.inputrc

### bash
if [[ -e ${HOME}/.bashrc ]]; then
    echo "backup .bashrc"
    ${COPY} ${HOME}/.bashrc ${BACKUP}/bashrc
fi
echo "install .bashrc"
${COPY} ${DOTFILES}/bashrc ${HOME}/.bashrc

# if [[ -e ${HOME}/.bash_profile ]]; then
#     echo "backup .bash_profile"
#     ${COPY} ${HOME}/.bash_profile ${BACKUP}/bash_profile
# fi
# echo "install .bash_profile"
# ${COPY} ${DOTFILES}/bash_profile ${HOME}/.bash_profile

if [[ -e ${HOME}/.bash_aliases ]]; then
    echo "backup .bash_aliases"
    ${COPY} ${HOME}/.bash_aliases ${BACKUP}/bash_aliases
fi
echo "install .bash_aliases"
${COPY} ${DOTFILES}/bash_aliases ${HOME}/.bash_aliases

echo "install liquidprompt"
if [ ! -e ~/.bash_liquidprompt ]; then
    ${MKDIR} ${DOTFILES}/src/
    git clone https://github.com/nojhan/liquidprompt.git ${DOTFILES}/src/liquidprompt
    ln -s ${DOTFILES}/src/liquidprompt/liquidprompt ~/.bash_liquidprompt
else
    echo "liquidprompt already installed ...."
fi

echo "install dircolors"
if [ ! -e ~/.bash_dircolors ]; then
    ${MKDIR} ${DOTFILES}/src/
    git clone https://github.com/seebi/dircolors-solarized ${DOTFILES}/src/dircolors
    ln -s ${DOTFILES}/src/dircolors/dircolors.256dark ~/.bash_dircolors
else
    echo "dircolors already installed ...."
fi

### neovim
echo "install neovim's init.vim (keeping installed plugins)"

if [[ -e ${HOME}/.config/nvim/init.vim ]]; then
    echo "backup nvim"
    ${COPY} -r ${HOME}/.config/nvim/init.vim ${BACKUP}/nvim
else
    ${MKDIR} -p ${HOME}/.config/nvim
fi

echo "install nvim"
${COPY} -r ${DOTFILES}/nvim/init.vim ${HOME}/.config/nvim/

### qutebrowser
if [[ -e ${HOME}/.config/qutebrowser/config.py ]]; then
    echo "backup qutebrowser"
    ${COPY} -r ${HOME}/.config/qutebrowser/config.py ${BACKUP}/qutebrowser
else
    ${MKDIR} -p ${HOME}/.config/qutebrowser
fi

echo "install qutebrowser"
${COPY} -r ${DOTFILES}/qutebrowser/config.py ${HOME}/.config/qutebrowser/

### i3
if [[ -e ${HOME}/.config/i3 ]]; then
    echo "backup i3"
    ${COPY} -r ${HOME}/.config/i3 ${BACKUP}/i3/
else
    ${MKDIR} -p ${HOME}/.config/i3
fi

echo "install i3"
${COPY} -r ${DOTFILES}/i3/* ${HOME}/.config/i3/

### solarized
# via tmux plugin
# git clone https://github.com/seebi/tmux-colors-solarized ${DOTFILES}/src/tmux-colors-solarized
# need keybinding for i3 to set xresources (which urxvt will use)
# git clone https://github.com/solarized/xresources ${DOTFILES}/src/xresources

### sshrc
echo "install sshrc (bash, tmux, nvim)"

${CAT} << EOF > ${HOME}/.sshrc
alias tmux='tmux -f \$SSHHOME/.sshrc.d/tmux.conf'
alias nvim='nvim -u \$SSHHOME/.sshrc.d/init.vim'
alias bash='INPUTRC=\$SSHHOME/.sshrc.d/inputrc bash -rcfile \$SSHHOME/.sshrc.d/bashrc'
EOF

if [[ ! -e ${HOME}/.sshrc.d ]]; then
    ${MKDIR} ${HOME}/.sshrc.d
fi

${COPY} -r ${DOTFILES}/nvim/init.vim ${HOME}/.sshrc.d/
${COPY} -r ${DOTFILES}/bashrc ${HOME}/.sshrc.d/
${COPY} -r ${DOTFILES}/tmux.conf ${HOME}/.sshrc.d/
${COPY} -r ${DOTFILES}/inputrc ${HOME}/.sshrc.d/

### gnupg
if [[ -e ${HOME}/.gnupg/ ]]; then
    echo "backup gnupg"
    ${MKDIR} ${BACKUP}/gnupg/
    ${COPY} ${HOME}/.gnupg/*.conf ${BACKUP}/gnupg/
else
    ${MKDIR} -p ${HOME}/.gnupg
fi

${COPY} ${DOTFILES}/gnupg/*.conf ${HOME}/.gnupg/

echo "set permissions for ${HOME}/.gnupg"
${CHMOD} 700 ${HOME}/.gnupg/
${CHMOD} 600 ${HOME}/.gnupg/*
