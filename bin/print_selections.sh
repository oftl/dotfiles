#!/bin/sh

PRIMARY=`/usr/bin/xclip -out -selection primary`
echo "primary: ${PRIMARY}"

echo "============================================="
SECONDARY=`/usr/bin/xclip -out -selection secondary`
echo "secondary: ${SECONDARY}"

echo "============================================="
CLIPBOARD=`/usr/bin/xclip -out -selection clipboard`
echo "clipboard: ${CLIPBOARD}"
