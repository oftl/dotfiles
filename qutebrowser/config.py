# pylint: disable=C0111
from qutebrowser.config.config import ConfigContainer  # noqa: F401
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401

config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

c.fonts.default_size = "18pt"
c.fonts.default_family = "monospace"

# c.fonts.completion.category = "bold 16pt monospace"
# c.fonts.completion.entry = "16pt monospace"
# c.fonts.debug_console = "16pt monospace"
# c.fonts.downloads = "16pt monospace"
# c.fonts.hints = "bold 16pt monospace"
# c.fonts.keyhint = "16pt monospace"
# c.fonts.messages.error = "16pt monospace"
# c.fonts.messages.info = "16pt monospace"
# c.fonts.messages.warning = "16pt monospace"
# # c.fonts.monospace = '"xos4 Terminus", Terminus, Monospace, "DejaVu Sans Mono", Monaco, "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", Courier, "Liberation Mono", monospace, Fixed, Consolas, Terminal'
# c.fonts.prompts = "16pt sans-serif"
# c.fonts.statusbar = "16pt monospace"
# c.fonts.tabs.selected = "16pt monospace"
# c.fonts.tabs.unselected = "16pt monospace"

c.statusbar.padding = {"top": 5, "bottom": 5, "left": 5, "right": 5}

c.zoom.default = "160%"

c.url.searchengines = {"DEFAULT": "https://duckduckgo.com/?q={}"}
# c.url.startpages = ["https://orf.at", "https://news.ycombinator.com"]
c.session.lazy_restore = True
c.auto_save.session = True
c.content.autoplay = False
# The content.pdfjs setting is not available with the QtWebEngine backend!
# c.content.pdfjs = True
c.downloads.position = "bottom"
c.editor.command = [
    "urxvt",
    "-e",
    "nvim",
    "-f",
    "{file}",
    "-c",
    "normal {line}G{column0}l",
]
c.tabs.background = True
c.tabs.mousewheel_switching = False

# key binding

config.unbind("d")

config.bind(",r", "config-source ~/.config/qutebrowser/config.py")

config.bind(",u", "yank --sel url")
config.bind(",o", "open -t -- {primary}")
config.bind(",.", "tab-close")

config.bind(",s", "view-source")

config.bind("<h>", "tab-prev")
config.bind("<l>", "tab-next")
config.bind("<Ctrl-h>", "tab-move -")
config.bind("<Ctrl-l>", "tab-move +")

config.bind(",b", "buffer")

config.bind(",i", "devtools left")
config.bind(",e", "open-editor")
