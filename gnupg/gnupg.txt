# GNUPGHOME=/tmp/gpgtest

# generate keys
gpg --full-generate-key

# export key
gpg --output <FILENAME> --armor --export user-id

# import key from file
gpg --import <FILENAME>

# push key to keyserver
gpg --send-keys user-id

# search/fetch key from keyserver
gpg --search-keys user-id
gpg --recv-keys key-id

# encrypt/decrypt
gpg --recipient user-id --encrypt doc
gpg --output doc --decrypt doc.gpg

# sign
gpg --output doc.sig --sign doc
gpg --output doc.sig --clearsign doc
gpg --output doc.sig --detach-sig doc

# verify
gpg --verify doc.sig

# gpg-agent, pinentry
# reload agent after eg. changes to config
gpg-connect-agent reloadagent /bye
