set timeoutlen=500

let mapleader = ','

" https://github.com/mhinz/vim-galore#go-to-other-end-of-selected-text

" n always searches forward
nnoremap <expr> n  'Nn'[v:searchforward]
xnoremap <expr> n  'Nn'[v:searchforward]
onoremap <expr> n  'Nn'[v:searchforward]

" N always searches bachward
nnoremap <expr> N  'nN'[v:searchforward]
xnoremap <expr> N  'nN'[v:searchforward]
onoremap <expr> N  'nN'[v:searchforward]

" match first few characters in command history (like up and down do)
cnoremap <c-n>  <down>
cnoremap <c-p>  <up>

" saner ctrl-l
nnoremap <leader>l :nohlsearch<cr>:diffupdate<cr>:syntax sync fromstart<cr><c-l>

" quit quick
noremap <Leader>. :quit<CR>
noremap <Leader>w :w<CR>
noremap <Leader><Leader> <c-^>

" switching windows
noremap <silent> <C-j> <C-w>j
noremap <silent> <C-k> <C-w>k
noremap <silent> <C-l> <C-w>l
noremap <silent> <C-h> <C-w>h

execute 'source' fnamemodify(expand('<sfile>'), ':h').'/config/main.vim'

" (un)highlight last search
noremap <silent>// :set hlsearch! hlsearch?<CR>

" autoformat neoformat
noremap <silent><leader>af :Neoformat<CR>

" autoformat black
noremap <silent><leader>ab :Black<CR>
