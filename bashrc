# bash's vi editing mode
set -o vi

# https://www.debian-administration.org/article/543/Bash_eternal_history
export HISTTIMEFORMAT="%s "
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND ; }"'echo $$ $USER "$(history 1)" >> ~/.bash_history_extended'

# PS1 is set and $- includes i if bash  is  interactive,
if [[ $- = *i* ]]; then
    echo -n 'Loading '
    if [ -e ~/.bash_liquidprompt ]; then
        echo -n 'liquidprompt'
        LP_ENABLE_TEMP=0 source ~/.bash_liquidprompt
    fi

    if [ -e ~/.bash_dir_colors ]; then
        echo -n ', dircolors'
        eval `dircolors ~/.bash_dir_colors`
    fi

    if [ -e ~/.fzf/ ]; then
        echo -n ', fzf'
        source ~/.fzf/shell/completion.bash
        source ~/.fzf/shell/key-bindings.bash
        # Requires highlight and tree: pacman -S highlight tree
        # export FZF_DEFAULT_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"
        export FZF_CTRL_T_OPTS="--height 65% --min-height 10 --preview-window top --preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"
        export FZF_CTRL_R_OPTS="--height 65% --sort"

        complete -o bashdefault -o default -F _fzf_path_completion zathura
    fi

    if [ -f ~/.bash_aliases ]; then
        echo -n ', aliases'
        source ~/.bash_aliases
    fi

    if [ -f ~/.bashrc.private ]; then
        echo -n ', bashrc.private '
        source ~/.bashrc.private
    fi

    if [ -f ~/.bashrc.work ]; then
        echo -n ', bashrc.work '
        source ~/.bashrc.work
    fi

    echo
fi

# https://wiki.archlinux.org/index.php/Color_output_in_console#less_2
export LESSOPEN="| /usr/bin/source-highlight-esc.sh %s"
export LESS='-R '

export HISTSIZE=5000

NVIM=`which nvim`
VIM=`which nvim`

if [[ -e ${NVIM} ]]; then
    export EDITOR=${NVIM}
elif [[ -e ${VIM} ]]; then
    export EDITOR=${VIM}
else
    export EDITOR=`which vi`
fi

export PAGER=less

# pass
export PASSWORD_STORE_X_SELECTION=primary

PATH=$PATH:${HOME}/bin/

# readline
set bell-style visible
