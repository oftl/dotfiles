# TODO
#
# alpine neovim package is missing python3
#
# RUN
#
# $ docker build --file Dockerfile --tag act .
# $ docker run --rm --interactive --tty true --volume ~:root act

FROM alpine
LABEL description="ACT - a coder's toolbox"
LABEL maintainer="oftl"

RUN apk update \
 && apk upgrade \
 && apk add \
    neovim \
    bash \
    git \
    tmux \
    xclip \
    curl \
    curl

RUN cd ~ \
 && mkdir ~/src \
 && git clone https://gitlab.com/oftl/dotfiles.git src/dotfiles \
 && cd ~/src/dotfiles/ \
 && sh install.sh

RUN nvim --headless +PlugInstall +qall
